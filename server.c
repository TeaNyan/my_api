#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include<netinet/in.h>
#include <errno.h>
#include <arpa/inet.h>
#include "api.h"
#define MAXDATASIZE 1000
#define MAXEVENTS 10

void dyn_buf_init (dyn_buf *x)
{
    if (x==NULL)
        return;
    x->buffer=NULL;
    x->size=0;
}
int create_and_bind (char *port)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s, sfd;
    
    memset(&hints, 0, sizeof (struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    
    s=getaddrinfo(NULL, port, &hints, &result);
    if (s!=0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -1;
    }
    
    for (rp=result; rp!=NULL; rp=rp->ai_next)
    {
        sfd=socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
            continue;
        
        s = bind(sfd, rp->ai_addr, rp->ai_addrlen);
        if (s==0)
            break;
    }
    
    if (rp == NULL)
    {
        printf("Could not bind\n");
        return -1;
    }
    
    freeaddrinfo(result);
    
    return sfd; 
}

int main(int argc, char *argv[])
{
    int sfd, s;
    int efd;
    struct epoll_event event;
    struct epoll_event *events;
    connection client_sock;
    dyn_buf_init(&client_sock.db);

    if (argc != 2)
    {
        fprintf (stderr, "Usage: %s [port]\n", argv[0]);
        exit (EXIT_FAILURE);
    }

    sfd = create_and_bind (argv[1]);
    if (sfd == -1)
        abort ();

    s = listen (sfd, SOMAXCONN);
    
    if (s == -1)
    {
        perror ("listen");
        abort ();
    }

    efd = epoll_create1 (0);
    if (efd == -1)
    {
        perror ("epoll_create");
        abort ();
    }

    event.data.fd = sfd;
    event.events = EPOLLIN | EPOLLET;
    s = epoll_ctl (efd, EPOLL_CTL_ADD, sfd, &event);
    if (s == -1)
    {
        perror ("epoll_ctl");
        abort ();
    }
    events = calloc (MAXEVENTS, sizeof event);
    
    int n, o;

    while (1)
    {
        n = epoll_wait (efd, events, MAXEVENTS, -1);
        for (o = 0; o < n; o++)
        {
            if ((events[o].events & EPOLLERR) ||
                    (events[o].events & EPOLLHUP) ||
                    (!(events[o].events & EPOLLIN)))
            {
                fprintf (stderr, "epoll error\n");
                close (events[o].data.fd);
                continue;
            }
            else if (sfd == events[o].data.fd)
            {
                struct sockaddr in_addr;
                socklen_t in_len;
                int infd;
                char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

                in_len = sizeof in_addr;
                infd = accept (sfd, &in_addr, &in_len);
                if (infd == -1)
                {
                    if ((errno == EAGAIN) ||
                            (errno == EWOULDBLOCK))
                        break;
                    else
                    {
                        perror ("accept");
                        break;
                    }
                }
                s = getnameinfo (&in_addr, in_len,
                                    hbuf, sizeof hbuf,
                                    sbuf, sizeof sbuf,
                                    NI_NUMERICHOST | NI_NUMERICSERV);
                if (s == 0)
                {
                    printf("Accepted connection on descriptor %d "
                            "(host=%s, port=%s)\n", infd, hbuf, sbuf);
                }
                
                connection *asdf = malloc(sizeof(connection));
                asdf->fd=infd;
                dyn_buf_init(&asdf->db);
                event.data.ptr=asdf;
                event.events = EPOLLIN | EPOLLET;
                s = epoll_ctl (efd, EPOLL_CTL_ADD, asdf->fd, &event);
                if (s == -1)
                {
                    perror ("epoll_ctl");
                    abort ();
                }
            }
            else
            {
                connection *asdf = events[o].data.ptr;
                operate(asdf, o);    
            }
    }
}
}
