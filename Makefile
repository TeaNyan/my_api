CC=clang
CFLAGS="-Wall"
CDEBUGFLAGS="-fsanitize=address"

debug:clean
	$(CC) $(CFLAGS) $(CDEBUGFLAGS) -g -o  server server.c api.c
stable:clean
	$(CC) $(CFLAGS) -o server server.c api.c
clean:
	rm -vfr *~ server

