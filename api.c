#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include "api.h"
#include<netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#define MAXDATASIZE 1000

int set(char *key, char *value);
int get(const char *findkey, char *buf);
int get_range(int sock, int x, int y);
int pop(const char *x);
int dyn_push(dyn_buf *db, void *buf, size_t size);
int dyn_pop (dyn_buf *x, size_t size);
int parse(dyn_buf *db, int i, int j, char* key);
int check_if_empty (connection *asdf, int i);
int check_key_size (connection *asdf, int i);
int check_if_key (connection *asdf);
int check_value_size (connection *asdf, int i);
int check_if_value (connection *asdf, int n);
int operate_pop (connection *asdf);
int operate_get (connection *asdf);
int operate_set (connection *asdf);
int operate_range (connection *asdf);

int set(char *key, char *value)
{
    char *buffer=NULL;
    size_t size=0;
    char *del=":";
    char *buf_del;
    char cc[50];
    int a=0;
    FILE *fp, *fp2;

    if ((fp=fopen("file.txt", "r+"))==NULL)
        return 0;
    rewind(fp);
    fp2=fopen("copy.txt", "w+");
    
    while (getline(&buffer, &size, fp)!=EOF)
    {
        strcpy(cc, buffer);
        buffer[strcspn(buffer, "\n")] = '\0';
        buf_del=strtok(buffer, del);
        
        if (!strcmp(buf_del, key))
        {
            fwrite(key, strlen(key), 1, fp2);
            fwrite(":", 1, 1, fp2);
            fwrite(value, strlen(value), 1, fp2);
            fwrite("\n", 1, 1, fp2);
            a=1;
        }
        else
            fwrite(cc, strlen(cc), 1, fp2);
        
    }
    
    if (a==0)
    {
        if (fwrite(key, strlen(key), 1, fp2)>0)
        if (fwrite(":", 1, 1, fp2)>0)
        if (fwrite(value, strlen(value), 1, fp2)>0)
        if (fwrite("\n", 1, 1, fp2)>0)
        fflush(fp2);
        a=1;
    }
        
    fclose(fp);
    fclose(fp2);
    remove("file.txt");
    rename("copy.txt", "file.txt");
    return a;
}
int get(const char *findkey, char *buf)
{
    char *del = ":";
    char *bufit;
    char* bs = NULL;
    size_t size=0;
    FILE *fp;
    
    fp=fopen("file.txt", "r+");
    while (getline(&bs, &size, fp) != -1)
    {
        bufit=strtok(bs, del);
        if (strcmp(bufit, findkey)==0)
        {
            bufit=strtok(NULL, del);
            if (bufit)
            {
            	strcpy(buf, bufit);
            	free(bs);
            	return 1;
            }
            else
            {
            	free(bs);
            	return -1;
            }
        }
    }
    free(bs);
    return 0;
}
int parse(dyn_buf *db, int i, int j, char* key)
{
    if (db->buffer[i]=='\n'||
        db->buffer[i]==' ' || 
        db->buffer[i]=='/')
            return 0;
    key[j]=db->buffer[i];
    key[j+1]='\0';
    return 1;
}
int get_range(int fd, int x, int y)
{
    char *buffer=NULL;
    size_t size=0;
    char *del = ":";
    FILE *fp;
    
    fp=fopen("file.txt", "r");
    fseek(fp, SEEK_SET, 0);
	int i = 1;
    
    for (; i < x; i++)
    {
		if (getline(&buffer, &size, fp) == -1)
		{
			free(buffer);
			return -1;
		}
    }
    for (; x <= y; x++)
    {
        if (getline(&buffer, &size, fp) != -1)
        {
            char* key;
            char* value;

            buffer[strcspn(buffer, "\n")] = '\0';
            key=strtok(buffer, del);
            value=strtok(NULL, del);
            dprintf(fd, "Key = %s, Value = %s\n", key, value);
        }
        else
            break;
    }

    free(buffer);
    return 1;
}
int pop(const char *x)
{
    char cc[50];
    char *del = ":";
    char *bufit;
    int a=0;
    FILE *fp, *fp2;
    
    fp=fopen("file.txt", "r+");
    fp2=fopen("copy.txt", "w");

    rewind(fp);

    char *buffer=NULL;
    size_t size=0;
    while (getline(&buffer, &size, fp)!=-1)
    {
        strcpy(cc, buffer);
        buffer[strcspn(buffer, "\n")] = '\0';
        bufit=strtok(buffer, del);

        if (bufit==NULL)
        {
            free(buffer);
            fclose(fp2);
            remove("copy.txt");
            return -1;
        }
        if (strcmp(bufit, x)!=0)
            fwrite(cc, strlen(cc), 1, fp2);
        else
            a=1;
    }   
    free(buffer);

    fclose(fp);
    fclose(fp2);
    remove("file.txt");
    rename("copy.txt", "file.txt");
    fp=fopen("file.txt", "r+");
    
    return a;   
}
int dyn_push(dyn_buf *db, void* buf, size_t size)
{
  if (db->buffer == NULL)
  {
    db->buffer=malloc(size);
    memcpy(db->buffer, buf, size);
    db->size=size;
  }
  else
  {
    char* new_mem=malloc(db->size+size);
    memcpy(new_mem, db->buffer, db->size);
    
    char* copy_new_mem =new_mem;
    memcpy(copy_new_mem + db->size, buf, size);
    
    free(db->buffer);
    db->buffer=new_mem;
    db->size+=size;

  }
  return size;
}
int dyn_pop(dyn_buf *db, size_t size)
{
    if (size>db->size)
        return -1;
    
    if (size==db->size)
    {
        free(db->buffer);
        db->buffer=NULL;
        db->size=0;
    }
    else
    {
        db->size-=size;
        char* new_buffer = malloc(db->size);
        memcpy(new_buffer, db->buffer + size, db->size);
        free(db->buffer);
        db->buffer=new_buffer;
    }
    
    return 1;
}

int operate (connection *asdf, int o)
{
    int i=0;
    size_t numbytes;
    char buf[MAXDATASIZE];

    if ((numbytes=recv(asdf->fd, 
        buf, MAXDATASIZE, 0)) == -1)
    {
        perror("recv");
        return 0;
    }
    dyn_push(&asdf->db, buf, numbytes);
        
    if (asdf->db.size<3)
        return 0;
    
    if (memcmp(asdf->db.buffer, "set", 3)==0)                                                           /* Begin of SET */
    {
        return operate_set(asdf);
    }
    else if (memcmp(asdf->db.buffer, "get", 3)==0)                                                       /* Begin of GET */
    {   
        return operate_get(asdf);
    }
    else if (memcmp(asdf->db.buffer, "ran", 3)==0)                                                         /* Begin of RANGE*/
    {
        return operate_range (asdf);
    }
    else if (memcmp(asdf->db.buffer, "pop", 3)==0)                                                          /* Begin of POP*/
    {
        return operate_pop(asdf);
    }
    else
    {
        dprintf(asdf->fd, "Invalid input. Expected '/' after value.\n");
        dyn_pop(&asdf->db, asdf->db.size);
        i=0;
        return 0;
    }
    return 0;
}
int check_if_empty (connection *asdf, int i)
{
    if (asdf->db.size>4)
    {
        for (i=0; i<=asdf->db.size; i++)
        {
            if (asdf->db.buffer[i]=='/')
                return 1;
        }
    }
    return 0;
}
int check_if_key (connection *asdf)
{
    if (asdf->db.buffer[4]=='/'||
        asdf->db.buffer[3]=='/')
    {
        dprintf(asdf->fd, "Invalid input. Expected key.\n");
        dyn_pop(&asdf->db, asdf->db.size);
        return 0;
    }
    return 1;
}
int check_if_value (connection *asdf, int n)
{
    if (asdf->db.buffer[n]=='/'||
        asdf->db.buffer[n+1]=='/')
    {
        dprintf(asdf->fd, "Invalid input. Expected value.\n");
        dyn_pop(&asdf->db, asdf->db.size);
        return 0;
    }
    return 1;
}
int check_key_size (connection *asdf, int i)
{
    if (i==30)
    {
        dprintf(asdf->fd, "Max key size is 30.\n");
        dyn_pop(&asdf->db, asdf->db.size);
        return 0;
    }
    return 1;
}
int check_value_size (connection *asdf, int i)
{
    if (i==100)
    {
        dprintf(asdf->fd, "Max value size is 100.\n");
        dyn_pop(&asdf->db, asdf->db.size);
        return 0;
    }
    return 1;
}
int operate_pop (connection *asdf)
{
    char key[30];
    int i, j, k;
    for (j=0, i=4; i<asdf->db.size; i++, j++)
    {
        if (check_key_size(asdf, i)!=1)
            return 0;
        if (check_if_key(asdf)!=1)
            return 0;
        if (asdf->db.buffer[i]=='\n')
            continue;
        k=parse(&asdf->db, i, j, key);
        if (k!=1)
            break;
    }
    if (i==asdf->db.size)
        return 0;
    if (memcmp(asdf->db.buffer +i, "/", 1)==0)
    {
        if (pop(key)!=1)
        {
            dprintf(asdf->fd, "Key does not exist!\n");
            dyn_pop(&asdf->db, i+2);
            return 0;
        }
        else
        {
            dprintf(asdf->fd, "Delete successful!\n");
            dyn_pop(&asdf->db, i+2);            i=0;
            return 0;
        }
        dyn_pop(&asdf->db, i+2);
        if (check_if_empty(asdf, i)==0)
            return 0;
            
        else
        {
            return 1;
        }
    }
    return 0;
}
int operate_range (connection *asdf)
{
    int rx=0, ry=0;
    char key[30], value[100];
    int i, j, k, n;

    for (i=4, j=0; i<asdf->db.size; i++, j++)
    {
        if (check_key_size(asdf, i)!=1)
            return 0;
        if (check_if_key(asdf)!=1)
            return 0;
        if (asdf->db.buffer[i]=='\n')
            break;
        if ((k=parse(&asdf->db, i, j, key)!=1))
            break;
    }
    if (i==asdf->db.size)
        return 0;
    key[j]='\0';
    rx=atoi(key);
    if (rx<=0)
    {
        dprintf(asdf->fd, "Key must be a number higher than 0\n");
        dyn_pop(&asdf->db, asdf->db.size);
        return 0;
    }
    n=i;
    
    for (i=i+1, j=0; i<asdf->db.size; i++, j++)
    {
        if (check_value_size(asdf, i)!=1)
            return 0;            
        if (check_if_value(asdf, n)!=1)
            return 0;
        if (asdf->db.buffer[i]=='\n')
            continue;
        if (asdf->db.buffer[i]==' ')
        {
            dprintf(asdf->fd, "Invalid input. Expected '/' after value.\n");
            dyn_pop(&asdf->db, asdf->db.size);
            return 0;               
        }
        if (asdf->db.buffer[i]=='/')
        {
            value[j]='\0';
            ry=atoi(value);
            if (ry<=0)
            {
                dprintf(asdf->fd, "Value must be a number higher than 0.\n");
                dyn_pop(&asdf->db, asdf->db.size);
            }                
            if (get_range(asdf->fd, rx, ry)==1)
            {
                dyn_pop(&asdf->db, i+2);
                if (check_if_empty(asdf, i)==0)
                    return 0;
                else
                    return 1;
            }
            else
            {
                dprintf(asdf->fd, "Get range failed");
                return 0;
            }
        }
        if ((k=parse(&asdf->db, i, j, value)!=1))
            break;
    }
    return 0;
}
int operate_get (connection *asdf)
{
    char key[30], value[100];
    int i, j, k;
    for (j=0, i=4; i<asdf->db.size; i++, j++)
    {
        if (check_key_size(asdf, i)!=1)
            return 0;
        if (check_if_key(asdf)!=1)
            return 0;
        if (asdf->db.buffer[i]=='\n')
            continue;
        if ((k=parse(&asdf->db, i, j, key)!=1))
            break;      
    }
    if (i==asdf->db.size)
        return 0;
    if (memcmp(asdf->db.buffer +i, "/", 1)==0)
    {
        
        if (get(key, value)!=1)
        {
            dprintf(asdf->fd, "Key does not exist!\n");
            dyn_pop(&asdf->db, i+2);
            i=0;
            return 0;
        }
        else
        {
            dprintf(asdf->fd, "Key: %s, Value: %s", key, value);
            dyn_pop(&asdf->db, i+2);
        }
        if (check_if_empty(asdf, i)==0)
            return 0;
    }
    else
    {
        dprintf(asdf->fd, "Invalid input. Required '/' after key.\n");
        dyn_pop(&asdf->db, asdf->db.size);
        return 0;
    }
    return 0;
}
int operate_set (connection *asdf)
{
    char buf [MAXDATASIZE], key[30], value[100];
    int i, j, k, a, n;
    
    for (j=0, i=4; i<asdf->db.size; i++, j++)
    {
        if (check_key_size(asdf, i)!=1)
            return 0;
        if (check_if_key(asdf)!=1)
            return 0;
        if ((k=parse(&asdf->db, i, j, key)!=1))
            break;
    }
    if (i==asdf->db.size)
        return 0;
    key[j]='\0';
    n=i;
    for (j=0, i=i+1; i<asdf->db.size; i++, j++)
    {
        if (check_value_size(asdf, i)!=1)
            return 0;
        if (check_if_value(asdf, n)!=1)
            return 0;
        if (asdf->db.buffer[i]=='\n')
        {
            value[j]=' ';
            continue;
        }
        if (memcmp(asdf->db.buffer +i, "/", 1)==0)
        {
            value[j]='\0';
            if (set(key, value)==1)
            {
                dprintf(asdf->fd, "Set successful!\n");
                dyn_pop(&asdf->db, i+2);
                memset(buf, 0, MAXDATASIZE);
                if (check_if_empty(asdf, i)==0)
                    return 0;
            }
            else
                dprintf(asdf->fd, "Set failed!\n");
            a=0;
            i=0;
            break;
        }
        value[j]=asdf->db.buffer[i];
    }
    if (i==asdf->db.size)
        return a;
    value[j]='\0';
    return a;
}



