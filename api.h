#ifndef API_H
#define API_H
#include <sys/epoll.h>
#include <bits/types.h>

typedef struct dyn_buf {
    char *buffer;
    size_t size;
}dyn_buf;

typedef struct connection {
    int fd;
    dyn_buf db;
}connection;

int operate (connection *asdf, int o);

#endif